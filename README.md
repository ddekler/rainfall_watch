# DHM Rainfall watch

This is a script to download river water levels from the 
[Department of Hydrology and Meteorology](https://www.dhm.gov.np/hydrology/hms-Single/171), Goverment of Nepal website.

## Usage

### Linux

To get the code, setup a virtual environment and install the required packages, run the following in a terminal:

```bash
git clone https://git.ecdf.ed.ac.uk/ddekler/rainfall_watch
cd rainfall_watch
virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
```

In subsequent sesions only 
```bash
cd rainfall_watch
. venv/bin/activate
```
are needed.

Run the script with the date of the start date timeseries as first argument:

```bash
./rainfall_watch.py 2024-04-01
```

This should produce `dhm_river_level_2470_20240401_1.csv` file containing the data as if the '1st of April 2024' and 'Period' was selected on the DHM website.

To get yesterday's data, use the `date` utility to format yesteray's date and feed it into the 
script:
```bash
./rainfall_watch.py $(date -d '-1 day' +%Y-%m-%d)
```

#### Downloading other timeseries

Other timeseries can be retrieved by specifiying the `-p X` flag with `X` an integer between `1` and `4`. The numbers correspond to
the following selection in the second dropdown menu on the website:

| `-p` | Dropdown    |
| ---  | ---         |
| 1    | Point       |
| 2    | Hourly      |
| 3    | Daily       |
| 4    | 7 Day Point |

For example:
```bash
./rainfall_watch.py -p 4 2024-04-01
```
will download the '7 Days Point' data to `dhm_river_level_2470_20240401_4.csv`.

#### Download the data for the last two weeks

The `rainfall_watch.sh` script takes an optional second argument to download data from a number of days ago.
The data for the last two weeks can be retreived with a for loop in a terminal:
```bash
for DAY in $(seq 1 14); do ./rainfall_watch.sh path/to/outptu_dir $DAY; done
```

#### As a `cron` job

A `cron` jab can be used to download the data every day at 9 am, by runnning:
```bash
crontab -e -u $USER
```
And add the following at the bottom of the file:
```
0 9 * * * /full/path/to/rainfall_watch.sh path/to/output_dir
```

### Windows

TODO

## Licence

MIT [licence](LICENCE).
