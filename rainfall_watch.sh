#!/bin/bash

# Run rainfall_watch.py as a cron job. See README.md for details.

SCRIPT_DIR=$(dirname $0)
DATA_DIR=$1
if [[ $# -eq 2 ]]; then
    DAYS_AGO=$2
else
    DAYS_AGO=1
fi

# Load the python environment
. $SCRIPT_DIR/venv/bin/activate

if [[ ! $? -eq 0 ]]; then
    #send_email "Could not activate virtual environment"
    echo "Could not source $SCRIPT_DIR/venv/bin/activate"
    exit 1
fi

# Dictionary mapping location names to seriesid obtained from the DMH website
declare -A seriesid
seriesid["Karnali"]=691
seriesid["Rapti"]=2470

# Dictionary mapping period names to numbers. See README.md for details.
declare -A period
period["Point"]=1
period["Hourly"]=2
period["Daily"]=3

# Retrieve data for each location
for LOCATION in "${!seriesid[@]}"; do

    LOCATION_ID=${seriesid[$LOCATION]}
    YESTERDAY_DATE=$(date -d "-$DAYS_AGO day" +%Y-%m-%d)

    # Run rainfall_watch.py for each period
    for PERIOD in "${!period[@]}"; do
        PERIOD_NUM=${period[$PERIOD]}

        OUTPUT_DIR="$DATA_DIR/$LOCATION/$PERIOD"
        # Create the final output directory if it does not exist
        if [[ ! -d  $OUTPUT_DIR ]]; then
            mkdir -p $OUTPUT_DIR
        fi

        # Scrape the data
        $SCRIPT_DIR/rainfall_watch.py -o $OUTPUT_DIR -i $LOCATION_ID -p $PERIOD_NUM $YESTERDAY_DATE

        # Wait before sending the next request
        sleep 1
    done
done
