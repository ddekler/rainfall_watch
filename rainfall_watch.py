#!/bin/env python

import argparse
import asyncio
import aiohttp
from bs4 import BeautifulSoup
import json
import os
import sys

async def request_dhm_river_data(session, date, period, series_id):
    '''Request a data table from the DHM (https://www.dhm.gov.np/site/getRiverWatchBySeriesId) website.

    :param session: aiohttp ClientSession object
    :param date: Start date of the time series in YYYY-MM-DD format.
    :param period: Period type, should be an integer between 1 and 4, where 
        1 is Point
        2 is Hourly
        3 is Daily
        4 is 7 Days Point
    :param river_id: To identify the river for which to get the time series.

    :returns: JSON
    '''
    URL='http://www.dhm.gov.np/site/getRiverWatchBySeriesId'

    data = {
        'date': date,
        'period': period,
        'seriesid': series_id,
    }

    async with session.post(URL, data=data) as response:
        r_text = await response.text()
        r_json = json.loads(r_text)
        #TODO: handle failures
        return r_json

def parse_html_table(table):
    '''Parse the HTML table from the DHM website into CSV

    :param table: HTML formatted table from the DHM website.

    :returns: String in CSV format.
    '''
    html = BeautifulSoup(table, features='html.parser')
    table_rows = html.table.select('tr')
    result = ""

    # Parse the heading (<tr><th> elements)
    for heading in table_rows[0].select('th'):
        result += '"' + heading.text.strip() + '",'
    result += "\n"

    # Parse each row with data (<tr><td> elements)
    for row in table_rows[1:]:
        for cell in row.select('td'):
            result += '"' + cell.text.strip() + '",'
        result += "\n"
    
    return result

def save_csv(filename, content):
    '''Writes data to a file

    :param filename: The name of the file
    :param content: Data to be saved
    '''

    try:
        with open(filename, 'w') as f:
            f.write(content)
    except:
        print(f"Could not open file for writing: {filename}. Check permissions and disk usage.", file=sys.stderr)
        exit(1)

async def amain(session, args):
    '''Async main'''

    filename = os.path.join(args.output_dir, f"dhm_river_level_{args.series_id}_{args.date}_{args.period_type}.csv")
    if os.path.exists(filename):
        print(f"Won't override existing file: {filename}", file=sys.stderr)
        exit(1)

    data = await request_dhm_river_data(session, args.date, args.period_type, args.series_id)
    csv_content = parse_html_table(data["data"]["table"])

    if len(csv_content.split('\n')) <= 2:
        print(f"No data recieved (series_id={args.series_id}, period_type={args.period_type}, date={args.date})", file=sys.stderr)
        exit(2)

    save_csv(filename, csv_content)

def main():
    '''
    'Dummy' main function that starts the aiohttp session and run asyncio.
    '''
    # Parse commandline arguments passed to the script
    parser = argparse.ArgumentParser(
        prog='rainfall_watch.py',
        description='Downloads river water level data from https://www.dhm.gov.np/hydrology/hms-Single/171'
    )
    parser.add_argument('-i', '--series_id', action='store', default=2470,
        help='Series ID [default=2470]'
    )
    parser.add_argument('-p', '--period_type', action='store', default=1,
        help='Period type (1 - point [default], 2 - hourly, 3 - daily, 4 - 7 Days Point)'
    )
    parser.add_argument('-o', '--output-dir', action='store', default='.',
        help='Directory where CSV files are saved to'
    )
    parser.add_argument('date', action='store',
        help='Start date of the time series in YYYY-DD-MM format'
    )
    args = parser.parse_args()

    # Run the async main function
    async def __start():
        async with aiohttp.ClientSession() as session:
            await amain(session, args)
    asyncio.run(__start())

if __name__ == "__main__":
    main()
